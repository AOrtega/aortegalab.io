export default [
  {
    name: "home",
    path: "/",
    component: () => import(/* webpackChunkName: "component--home" */ "/home/adrian/vue/adrian-ortega/src/pages/Index.vue"),
    meta: { isStatic: true }
  },
  {
    name: "404",
    path: "/404",
    component: () => import(/* webpackChunkName: "component--404" */ "/home/adrian/vue/adrian-ortega/node_modules/gridsome/app/pages/404.vue"),
    meta: { isIndex: false }
  },
  {
    name: "blog",
    path: "/blog/:page(\\d+)?",
    component: () => import(/* webpackChunkName: "component--blog" */ "/home/adrian/vue/adrian-ortega/src/pages/Blog.vue")
  },
  {
    path: "/blog/first-post",
    component: () => import(/* webpackChunkName: "component--post" */ "/home/adrian/vue/adrian-ortega/src/templates/Post.vue")
  },
  {
    path: "/blog/awesome-post",
    component: () => import(/* webpackChunkName: "component--post" */ "/home/adrian/vue/adrian-ortega/src/templates/Post.vue")
  },
  {
    path: "/blog/one-more",
    component: () => import(/* webpackChunkName: "component--post" */ "/home/adrian/vue/adrian-ortega/src/templates/Post.vue")
  },
  {
    path: "/blog/another-awesome-post",
    component: () => import(/* webpackChunkName: "component--post" */ "/home/adrian/vue/adrian-ortega/src/templates/Post.vue")
  },
  {
    name: "*",
    path: "*",
    component: () => import(/* webpackChunkName: "component--404" */ "/home/adrian/vue/adrian-ortega/node_modules/gridsome/app/pages/404.vue"),
    meta: { isIndex: false }
  }
]

